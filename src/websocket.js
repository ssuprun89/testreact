import React from 'react';
import App from "./App";
import './App.css';

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ws: null
        };
    }

    componentDidMount() {
        this.connect();
    }

    /**
     * @function connect
     * This function establishes the connect with the websocket and also ensures constant reconnection if connection closes
     */
    connect = () => {
        var api_key = '5Z4JDQPKL82E5CO1A0L0';
        var ws = new WebSocket('wss://stream.cryptowat.ch/connect?apikey='+api_key);
        let that = this; // cache the this

        // websocket onopen event listener
        ws.onopen = () => {
            console.log("connected websocket main component");
            this.setState({ ws: ws });
        };

        // websocket onclose event listener
        ws.onclose = e => {
            console.log(
                `Socket is closed. Reconnect will be attempted in ${Math.min(
                    10000 / 1000,
                    (that.timeout + that.timeout) / 1000
                )} second.`,
                e.reason
            );
        };

        // websocket onerror event listener
        ws.onerror = err => {
            console.error(
                "Socket encountered error: ",
                err.message,
                "Closing socket"
            );
            ws.close();
        };
    };

    /**
     * utilited by the @function connect to check if the connection is close, if so attempts to reconnect
     */
    check = () => {
        const { ws } = this.state;
        if (!ws || ws.readyState == WebSocket.CLOSED) this.connect(); //check if websocket instance is closed, if so call `connect` function.
    };

    render() {
        return <App websocket={this.state.ws} />;
    }
}
export default Main;
