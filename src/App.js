import React from 'react';
import './App.css';
import Tr_body from "./tr_body";
import P_logs from "./p_logs";
class App extends React.Component {

    state = {
        filter:'',
        messages: [],
    }

    api_key = '5Z4JDQPKL82E5CO1A0L0';
    ws = new WebSocket('wss://stream.cryptowat.ch/connect?apikey='+this.api_key);

    componentDidMount() {
        var that = this
        this.ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
        }

        this.ws.onmessage = function(event){
            let d= blobToString(event.data); // convert blob to JSON
            if (d.authenticationResult && d.authenticationResult.status === 'AUTHENTICATED') {
                subscribeTo(this, ['instruments:9:trades']); // get data from server
            }
            //check JSON response on actual data
            if (d.marketUpdate && d.marketUpdate.tradesUpdate) {
                for (let trade of d.marketUpdate.tradesUpdate.trades) {
                    that.addMessage(d.marketUpdate.market.marketId, trade)
                }
            }
        }

        function subscribeTo(conn, resources) {
            // send message on server
            conn.send(JSON.stringify({
                subscribe: {
                    subscriptions: resources.map((resource) => {
                        console.log(resource);
                        return { streamSubscription: { resource: resource } }
                    })
                }
            }));
        }

        function blobToString(b) {
            let u = URL.createObjectURL(b);
            let x = new XMLHttpRequest();
            x.open('GET', u, false);
            x.send();
            URL.revokeObjectURL(u);
            return JSON.parse(x.responseText);
        }


        this.ws.onclose = () => {
            console.log('disconnected')
            // automatically try to reconnect on connection loss
        }
    }

    addMessage = (market, message) =>{
        let val = this.state.filter
        if (val == message.market || val == '') {
            message.hide = ''
        } else {
            message.hide = 'hide'
        }
        message['market'] = market
        this.setState(state => ({ messages: [message, ...state.messages] }))
    }

    filter = (val) => {
        this.setState(state => {
            let mess = state.messages.map((message, index) => {
                if (val == message.market || val == '') {
                    message.hide = ''
                } else {
                    message.hide = 'hide'
                }
            })
            return {mess}
        })
        this.setState({filter:val} )
    }

    render() {
        return (<div id="main">
                <div id="table">
                    <div><input type="number" id="filter" onKeyUp={e => this.filter(e.target.value)} onChange={e => this.filter(e.target.value)}/></div>
                    <table style={{width: '100%'}}>
                        <thead>
                        <tr>
                            <th>
                                Market
                            </th>
                            <th>
                                Price USD
                            </th>
                            <th>
                                Amount BTC
                            </th>
                        </tr>
                        </thead>
                        <tbody id="tbody_elem">
                        {this.state.messages.map((message, index) =>
                            <Tr_body
                                key={index}
                                amount={message.amountStr}
                                price={message.priceStr}
                                market={message.market}
                                hide={message.hide}
                            />,
                        )}
                        </tbody>
                    </table>
                </div>
                <div id="logs">
                    {this.state.messages.map((message, index) =>
                        <P_logs
                            key={index}
                            amount={message.amountStr}
                            price={message.priceStr}
                            market={message.market}
                            time={message.timestamp}
                        />,
                    )}
                </div>
            </div>)
    }
}

export default App;
