import React from 'react'

export default ({ market,  price, amount, hide}) =>{
    return (<tr className={hide}>
                <td>{market}</td>
                <td>{price}</td>
                <td>{amount}</td>
            </tr>)}
