import React from 'react'

export default ({ market,  price, amount, time}) =>{
    let date = new Date(parseInt(time)).toString().split('(')[0]
    let sting_logs = `BTC/USD trade on market ${market}: ${date} ${price} USD ${amount} btc`;
    return <p className={'p_logs'}>{sting_logs}</p>
}
